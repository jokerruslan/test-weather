﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Weather.Domain.Enums;
using Weather.Domain.Models;

namespace Weather.Domain
{
	public interface IWeatherService
	{
		Task<WeatherItem> GetCurrentWeatherAsync(string cityName, UnitsEnum units,LanguageEnum lang);

		Task<IEnumerable<WeatherItem>> GetDetailedWeatherForecastAsync(string cityName, UnitsEnum units,LanguageEnum lang);

		Task<IEnumerable<WeatherItem>> GetDayAverageWeatherForecastAsync(string cityName, UnitsEnum units, LanguageEnum lang);
	}
}
