﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Domain
{
	public interface IDataMapper
	{
		T2 Map<T1, T2>(T1 currentWeather);
	}
}
