﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Domain.CustomExceptions
{
	public class OpenWeatherHttpException : Exception
	{
		public OpenWeatherHttpException(string message, int statusCode) : base(message)
		{
		}
	}
}
