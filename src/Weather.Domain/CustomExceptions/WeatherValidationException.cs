﻿using System;

namespace Weather.Domain.CustomExceptions
{
	public class WeatherValidationException : Exception
	{
		public WeatherValidationException(string message) : base(message)
		{
		}
	}
}
