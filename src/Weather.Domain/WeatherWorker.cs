﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weather.Common;
using Weather.Domain.CustomExceptions;
using Weather.Domain.Dto;
using Weather.Domain.Enums;
using Weather.Domain.Models;

namespace Weather.Domain
{
	public class WeatherWorker
	{
		private readonly IDataMapper _dataMapper;
		private readonly IWeatherService _weatherService;

		public WeatherWorker(IWeatherService weatherService, IDataMapper dataMapper)
		{
			_dataMapper = dataMapper ?? throw new ArgumentNullException("IDataMapper is null");
			_weatherService = weatherService ?? throw new ArgumentNullException("IWeatherService is null");
		}

		public async Task<ResultDto<WeatherDto>> GetWeatherAsync(string city, UnitsEnum units, LanguageEnum lang, SortColumn sortColumn, SortOrderEnum sortOrder)
		{
			var result = new ResultDto<WeatherDto>();

			try
			{
				var currentWeatherTask = _weatherService.GetCurrentWeatherAsync(city, units, lang);
				var forecastWeatherTask = _weatherService.GetDayAverageWeatherForecastAsync(city, units, lang);

				var currentWeather = await currentWeatherTask;
				var forecastWeather = await forecastWeatherTask;

				if (sortOrder != SortOrderEnum.Unspecified)
				{
					var func = forecastWeather.OrderByColumn(sortColumn.GetDescription());

					forecastWeather =
						sortOrder == SortOrderEnum.Ascending
						? forecastWeather.OrderBy(func) :
						forecastWeather.OrderByDescending(func);
				}
				
				result.Result = new WeatherDto()
				{
					City= _dataMapper.Map<WeatherItem, CityDto>(currentWeather),
					Current = _dataMapper.Map<WeatherItem, WeatherItemDto>(currentWeather),
					Forecasts = _dataMapper.Map<IEnumerable<WeatherItem>, IEnumerable<WeatherItemDto>>(forecastWeather),
				};

				result.Success = true;
			}
			catch (WeatherValidationException e)
			{
				result.Message = e.Message;
			}
			catch (OpenWeatherHttpException e)
			{
				result.Message = e.Message;
			}
			catch (Exception e)
			{
				result.Message = CustomMessagesHelper.UnexpectedError;
			}

			return result;
		}
	}
}
