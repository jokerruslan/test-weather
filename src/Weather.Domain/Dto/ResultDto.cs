﻿
namespace Weather.Domain.Dto
{
	public class ResultDto<T>
	{
		public T Result { get; set; }
		public string Message { get; set; }
		public bool Success { get; set; }
	}
}
