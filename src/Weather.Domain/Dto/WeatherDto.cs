﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Domain.Dto
{
	public class WeatherDto
	{
		public WeatherDto()
		{
			Forecasts = new List<WeatherItemDto>();
		}

		public WeatherItemDto Current { get; set; }

		public IEnumerable<WeatherItemDto> Forecasts { get; set; }

		public CityDto City { get; set; }
	}
}
