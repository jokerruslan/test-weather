﻿using System.ComponentModel;

namespace Weather.Domain.Enums
{
	public enum LanguageEnum
	{
		[Description("ru")]
		ru,
		[Description("en")]
		en
	}
}
