﻿using System.ComponentModel;

namespace Weather.Domain.Enums
{
	public enum UnitsEnum
	{
		[Description("kelvin")]
		Kelvin = 0,
		[Description("metric")]
		Celsius = 1,
		[Description("imperial")]
		Fahrenheits = 2
	}
}
