﻿using System.ComponentModel;
using Weather.Domain.Models;

namespace Weather.Domain.Enums
{
	public enum SortColumn
	{
		[Description(nameof(WeatherItem.Date))]
		Date,
		[Description(nameof(WeatherItem.Temperature))]
		Temperature,
		[Description(nameof(WeatherItem.MinimumTemperature))]
		MinimumTemperature,
		[Description(nameof(WeatherItem.MaximumTemperature))]
		MaximumTemperature,
		[Description(nameof(WeatherItem.Pressure))]
		Pressure,
		[Description(nameof(WeatherItem.Humidity))]
		Humidity,
		[Description(nameof(WeatherItem.WindSpeed))]
		WindSpeed,
		[Description(nameof(WeatherItem.WindDirectionDegrees))]
		WindDirectionDegrees,
		[Description(nameof(WeatherItem.Cloudiness))]
		Cloudiness,
		[Description(nameof(WeatherItem.Units))]
		Units
	}
}
