﻿namespace Weather.Domain.Enums
{
	public enum SortOrderEnum
	{
		Unspecified = -1,
		Ascending = 0,
		Descending = 1
	}
}
