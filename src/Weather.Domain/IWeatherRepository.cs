﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Weather.Domain.Dto;

namespace Weather.Domain
{
	public interface IWeatherRepository
	{
		Task<ResultDto<bool>> SaveWeatherAsync(WeatherDto weatherDto);
	}
}
