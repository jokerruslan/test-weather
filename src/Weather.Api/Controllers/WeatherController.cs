﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Weather.Domain;
using Weather.Domain.Dto;
using Weather.Domain.Enums;

namespace Weather.Api.Controllers
{
	[Route("api/weather")]
	public class WeatherController : BaseApiController
	{
		private readonly IWeatherRepository _weatherRepository;
		private readonly WeatherWorker _weatherWorker;

		public WeatherController(WeatherWorker weatherWorker, IWeatherRepository weatherRepository)
		{
			_weatherRepository= weatherRepository?? throw new ArgumentNullException("IWeatherRepository is null");
			_weatherWorker = weatherWorker ?? throw new ArgumentNullException("WeatherWorker is null");
		}

		[HttpGet]
		public async Task<ActionResult<ResultDto<WeatherDto>>> Get(
			string city,
			UnitsEnum units,
			LanguageEnum lang=LanguageEnum.en,
			SortColumn sortColumn = SortColumn.Date,
			SortOrderEnum sortOrder = SortOrderEnum.Unspecified)
		{
			var result =await _weatherWorker.GetWeatherAsync(city, units, lang, sortColumn, sortOrder);

			return ResponseResult(result);
		}

		[HttpPost]
		public async Task<ActionResult<ResultDto<bool>>> Post(WeatherDto model)
		{
			var result = await _weatherRepository.SaveWeatherAsync(model);

			return ResponseResult(result);
		}
	}
}
