﻿
using Microsoft.AspNetCore.Mvc;
using Weather.Domain.Dto;

namespace Weather.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BaseApiController : ControllerBase
    {
		protected ActionResult<ResultDto<T>> ResponseResult<T>(ResultDto<T> result)
		{
			if (!string.IsNullOrEmpty(result.Message))
			{
				return BadRequest(result);
			}
			
			return Ok(result);
		}
	}
}