﻿using AutoMapper;
using System;
using System.Linq;
using Weather.Domain.Dto;
using Weather.Domain.Models;
using Weather.Infrastructure;

namespace Weather.Api.Mapping
{
	internal class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<OpenWeatherItem, WeatherItem>()
				.ForMember(dest => dest.Date, opt => opt.MapFrom(x => DateTimeOffset.FromUnixTimeSeconds(x.UnixTimestamp).UtcDateTime))
				.ForMember(dest => dest.Temperature, opt => opt.MapFrom(x => x.Main.Temp))
				.ForMember(dest => dest.Pressure, opt => opt.MapFrom(x => x.Main.Pressure))
				.ForMember(dest => dest.Humidity, opt => opt.MapFrom(x => x.Main.Humidity))
				.ForMember(dest => dest.MinimumTemperature, opt => opt.MapFrom(x => x.Main.TempMin))
				.ForMember(dest => dest.MaximumTemperature, opt => opt.MapFrom(x => x.Main.TempMax))
				.ForMember(dest => dest.WindSpeed, opt => opt.MapFrom(x => x.Wind.Speed))
				.ForMember(dest => dest.WindDirectionDegrees, opt => opt.MapFrom(x => x.Wind.DirectionDegrees))
				.ForMember(dest => dest.Cloudiness, opt => opt.MapFrom(x => x.Clouds.Cloudiness))
				.ForMember(dest => dest.Description, opt => opt.MapFrom(x => string.Join(",", x.Weather.Select(a=>a.Description).Distinct())));

			CreateMap<WeatherItem, CityDto>()
				.ForMember(dest => dest.Id, opt => opt.MapFrom(x => x.CityId))
				.ForMember(dest => dest.Name, opt => opt.MapFrom(x => x.CityName));


		}
	}
}
