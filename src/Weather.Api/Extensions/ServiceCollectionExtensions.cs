﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Weather.Api.Mapping;
using Weather.Common;
using Weather.Data;
using Weather.Domain;
using Weather.Infrastructure;
using Weather.Infrastructure.Services;
using Weather.Repository;

namespace Weather.Api.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddMapper(this IServiceCollection services)
		{
			var mappingConfig = new MapperConfiguration(mc =>
			{
				mc.AddProfile(new MappingProfile());
			});

			IMapper mapper = mappingConfig.CreateMapper();

			services.AddSingleton(mapper);

			return services;
		}

		public static IServiceCollection AddWeatherDi(this IServiceCollection services)
		{
			services.AddScoped<WeatherWorker>();

			services.AddScoped<IWeatherService, WeatherService>();

			services.AddScoped<IWeatherProvider, WeatherProvider>();
			services.AddScoped<IDataMapper, DataMapper>();

			services.AddScoped<OpenWeatherProvider>();

			return services;
		}

		public static IServiceCollection AddHttpClient(this IServiceCollection services, IConfiguration configuration)
		{
			var appConfig = new AppConfig();

			configuration.Bind(appConfig);

			services.AddHttpClient(ApiClientHelper.WeatherClient, c =>
			{
				c.BaseAddress = new Uri($"{appConfig.BaseApiUrl}/weather?appId={appConfig.AppId}");
			});

			services.AddHttpClient(ApiClientHelper.ForecastClient, c =>
			{
				c.BaseAddress = new Uri($"{appConfig.BaseApiUrl}/forecast?appId={appConfig.AppId}");
			});

			return services;
		}

		public static IServiceCollection AddRepository(this IServiceCollection services)
		{
			services.AddScoped<IReadOnlyRepository, ReadOnlyRepository>();
			services.AddScoped<EntityFrameworkRepository>();
			services.AddScoped<DbContext, ApplicationDbContext>();
			services.AddScoped<IWeatherRepository, WeatherRepository>();

			return services;
		}



	}
}
