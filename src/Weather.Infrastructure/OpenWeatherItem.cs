﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Weather.Infrastructure
{
	public class OpenWeatherItem
	{
		public OpenWeatherItem()
		{
			Weather = new List<WeatherPart>();
		}
		[JsonProperty("main")]
		public MainPart Main { get; set; }

		[JsonProperty("dt")]
		public long UnixTimestamp { get; set; }

		[JsonProperty("wind")]
		public WindPart Wind { get; set; }

		[JsonProperty("clouds")]
		public CloudsPart Clouds { get; set; }

		[JsonProperty("weather")]
		public IEnumerable<WeatherPart> Weather { get; set; }

		[JsonProperty("id")]
		public int CityId { get; set; }

		[JsonProperty("name")]
		public string CityName { get; set; }
	}

	public class CityPart
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}

	public class WeatherPart
	{
		[JsonProperty("description")]
		public string Description { get; set; }
	}

	public class MainPart
	{
		[JsonProperty("temp")]
		public double Temp { get; set; }

		[JsonProperty("pressure")]
		public double Pressure { get; set; }

		[JsonProperty("humidity")]
		public int Humidity { get; set; }

		[JsonProperty("temp_min")]
		public double? TempMin { get; set; }

		[JsonProperty("temp_max")]
		public double? TempMax { get; set; }
	}

	public class WindPart
	{
		[JsonProperty("speed")]
		public double Speed { get; set; }

		[JsonProperty("deg")]
		public double DirectionDegrees { get; set; }
	}

	public class CloudsPart
	{
		[JsonProperty("all")]
		public double Cloudiness { get; set; }
	}
}
