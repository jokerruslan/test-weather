﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Weather.Common;
using Weather.Domain.CustomExceptions;
using Weather.Domain.Enums;

namespace Weather.Infrastructure
{
	public class OpenWeatherProvider
	{
		private readonly ILogger<OpenWeatherProvider> _logger;
		private readonly IHttpClientFactory _httpClientFactory;

		public OpenWeatherProvider(IHttpClientFactory httpClientFactory, ILogger<OpenWeatherProvider> logger)
		{
			_httpClientFactory = httpClientFactory ?? throw new ArgumentNullException("IHttpClientFactory is null");
			_logger = logger ?? throw new ArgumentNullException("IHttpClientFactory is null");
		}

		internal Task<OpenWeatherItem> GetCurrentWeatherAsync(string city, UnitsEnum units, LanguageEnum lang)
		{
			return GetData<OpenWeatherItem>(ApiClientHelper.WeatherClient, city, units, lang);
		}

		internal Task<OpenWeatherForecast> GetForecastWeatherAsync(string city, UnitsEnum units, LanguageEnum lang)
		{
			return GetData<OpenWeatherForecast>(ApiClientHelper.ForecastClient, city, units, lang);
		}

		private async Task<T> GetData<T>(string clientName, string city, UnitsEnum units, LanguageEnum lang) where T : class
		{
			if (string.IsNullOrWhiteSpace(city))
				throw new WeatherValidationException(CustomMessagesHelper.ErrorCity);

			HttpResponseMessage response = null;
			try
			{
				var unitsParam = string.Empty;
				if (units != UnitsEnum.Kelvin)
					unitsParam = $"&units={units.GetDescription()}";

				var httpClient = _httpClientFactory.CreateClient(clientName);

				var uri = new Uri($"{httpClient.BaseAddress}&q={city}{unitsParam}&lang={lang.GetDescription()}");

				response = await httpClient.GetAsync(uri);

				response.EnsureSuccessStatusCode();

				var strResult = await response.Content.ReadAsStringAsync();

				var currentWeather = JsonConvert.DeserializeObject<T>(strResult);

				return currentWeather;
			}
			catch (HttpRequestException e)
			{
				_logger.LogError(e.ToString());
				if (response == null || CustomMessagesHelper.IsServerError(response.StatusCode))
					throw new OpenWeatherHttpException(CustomMessagesHelper.ErrorServerUnavailable, 500);

				if (response.StatusCode == HttpStatusCode.NotFound)
					throw new OpenWeatherHttpException(CustomMessagesHelper.ErrorCityNotFound, 404);

				throw;
			}
			catch (AggregateException e)
			{
				_logger.LogError(e.ToString());
				throw;
			}
			catch (Exception e)
			{
				_logger.LogError(e, CustomMessagesHelper.UnexpectedError);
				throw;
			}
		}
	}
}
