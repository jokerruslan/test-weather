﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Weather.Domain;
using Weather.Domain.Enums;
using Weather.Domain.Models;

namespace Weather.Infrastructure
{
	public class WeatherProvider : IWeatherProvider
	{
		private readonly OpenWeatherProvider _openWeatherProvider;
		private readonly IDataMapper _dataMapper;

		public WeatherProvider(OpenWeatherProvider openWeatherProvider, IDataMapper dataMapper)
		{
			_openWeatherProvider = openWeatherProvider?? throw new ArgumentNullException("OpenWeatherProvider is null");
			_dataMapper = dataMapper ?? throw new ArgumentNullException("IDataMapper is null"); ;
		}
		public async Task<WeatherItem> GetCurrentWeatherAsync(string cityName, UnitsEnum units, LanguageEnum lang)
		{
			var data= await _openWeatherProvider.GetCurrentWeatherAsync(cityName, units, lang);

			return _dataMapper.Map<OpenWeatherItem,WeatherItem>(data);
		}

		public async Task<IEnumerable<WeatherItem>> GetForecastWeatherAsync(string cityName, UnitsEnum units, LanguageEnum lang)
		{
			var data = await _openWeatherProvider.GetForecastWeatherAsync(cityName, units, lang);

			return _dataMapper.Map<IEnumerable<OpenWeatherItem>, IEnumerable<WeatherItem>>(data.Items);
		}
	}
}
