﻿using System;
using System.Threading.Tasks;
using Weather.Domain.Dto;
using Weather.Repository;

namespace Weather.Domain
{
	public class WeatherRepository: IWeatherRepository
	{
		private IDataMapper _dataMapper;
		private readonly EntityFrameworkRepository _entityFrameworkRepository;

		public WeatherRepository(EntityFrameworkRepository entityFrameworkRepository, IDataMapper dataMapper)
		{
			_dataMapper = dataMapper ?? throw new ArgumentNullException("EntityFrameworkRepository is null");
			_entityFrameworkRepository = entityFrameworkRepository ?? throw new ArgumentNullException("EntityFrameworkRepository is null");
		}

		public async Task<ResultDto<bool>> SaveWeatherAsync(WeatherDto weatherDto)
		{
			//TODO implement all mapping to entities here
			//TODO implement error handling here

			return new ResultDto<bool>()
			{
				Success=true,
				Result=true
			};
		}
	}
}
