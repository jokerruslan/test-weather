﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Weather.Domain;
using Weather.Domain.Enums;
using Weather.Domain.Models;

namespace Weather.Infrastructure.Services
{
	public class WeatherService : IWeatherService
	{
		private readonly IWeatherProvider _weatherProvider;

		public WeatherService(IWeatherProvider weatherProvider)
		{
			_weatherProvider = weatherProvider ?? throw new ArgumentNullException("IWeatherProvider is null");
		}

		public Task<WeatherItem> GetCurrentWeatherAsync(string cityName, UnitsEnum units, LanguageEnum lang)
		{
			return _weatherProvider.GetCurrentWeatherAsync(cityName, units,lang);
		}

		public async Task<IEnumerable<WeatherItem>> GetDayAverageWeatherForecastAsync(string cityName, UnitsEnum units, LanguageEnum lang)
		{
			var detailedForecast = await _weatherProvider.GetForecastWeatherAsync(cityName, units, lang);

			var result = detailedForecast.GroupBy(a => a.Date.Date)
				.Select(a => new WeatherItem()
				{
					Date = a.Key,
					Temperature = a.Average(x => x.Temperature),
					MinimumTemperature = a.Average(x => x.MinimumTemperature),
					MaximumTemperature = a.Average(x => x.MaximumTemperature),
					Humidity = (int)Math.Round(a.Average(x => x.Humidity)),
					Cloudiness = (int)Math.Round(a.Average(x => x.Cloudiness)),
					Pressure = a.Average(x => x.Pressure),
					WindSpeed = a.Average(x => x.WindSpeed),
					WindDirectionDegrees = a.Average(x => x.WindDirectionDegrees),
					Description=string.Join(",",a.Select(t=>t.Description).Distinct())
				});

			return result;
		}

		public Task<IEnumerable<WeatherItem>> GetDetailedWeatherForecastAsync(string cityName, UnitsEnum units, LanguageEnum lang)
		{
			return _weatherProvider.GetForecastWeatherAsync(cityName, units, lang);
		}
	}
}
