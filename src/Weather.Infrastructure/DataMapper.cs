﻿using AutoMapper;
using System;
using Weather.Domain;

namespace Weather.Infrastructure
{
	public class DataMapper : IDataMapper
	{
		private readonly IMapper _mapper;

		public DataMapper(IMapper mapper)
		{
			_mapper = mapper?? throw new ArgumentNullException("IMapper is null");
		}

		public T2 Map<T1, T2>(T1 obj)
		{
			return _mapper.Map<T1, T2>(obj);
		}
	}
}
