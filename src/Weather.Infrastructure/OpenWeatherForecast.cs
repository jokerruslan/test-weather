﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Infrastructure
{
	public class OpenWeatherForecast
	{
		public OpenWeatherForecast()
		{
			Items = new List<OpenWeatherItem>();
		}
		[JsonProperty("list")]
		public IEnumerable<OpenWeatherItem> Items { get; set; }

		[JsonProperty("city")]
		public CityPart City { get; set; }
	}
}
