﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Weather.Domain.Enums;
using Weather.Domain.Models;

namespace Weather.Infrastructure
{
	public interface IWeatherProvider
	{
		Task<WeatherItem> GetCurrentWeatherAsync(string cityName, UnitsEnum units,LanguageEnum lang);

		Task<IEnumerable<WeatherItem>> GetForecastWeatherAsync(string cityName, UnitsEnum units, LanguageEnum lang);
	}
}
