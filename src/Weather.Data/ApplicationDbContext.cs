﻿using Microsoft.EntityFrameworkCore;
using Weather.Domain.Entities;

namespace Weather.Data
{
	public class ApplicationDbContext:DbContext
	{
		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> dbContextOptions)
			  : base(dbContextOptions)
		{
		}
		//TODO add tables for forecasts(one to many)
		public DbSet<City> City { get; set; }
	}
}
