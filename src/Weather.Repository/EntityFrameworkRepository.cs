﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Weather.Repository
{
	public class EntityFrameworkRepository : ReadOnlyRepository, IRepository
	{
		private readonly DbContext _context;

		public EntityFrameworkRepository(DbContext context) : base(context)
		{
			_context = context;
		}

		public virtual void Create<TEntity>(TEntity entity, string createdBy = null)
			where TEntity : class, IEntity
		{
			_context.Set<TEntity>().Add(entity);
		}

		public virtual void Update<TEntity>(TEntity entity, string modifiedBy = null)
			where TEntity : class, IEntity
		{
			_context.Set<TEntity>().Attach(entity);
			_context.Entry(entity).State = EntityState.Modified;
		}

		public virtual void Delete<TEntity>(object id)
			where TEntity : class, IEntity
		{
			TEntity entity = _context.Set<TEntity>().Find(id);
			Delete(entity);
		}

		public virtual void Delete<TEntity>(TEntity entity)
			where TEntity : class, IEntity
		{
			var dbSet = _context.Set<TEntity>();
			if (_context.Entry(entity).State == EntityState.Detached)
			{
				dbSet.Attach(entity);
			}
			dbSet.Remove(entity);
		}

		public virtual int Save()
		{
			return _context.SaveChanges();
		}

		public virtual Task SaveAsync()
		{
			return _context.SaveChangesAsync();
		}
	}
}