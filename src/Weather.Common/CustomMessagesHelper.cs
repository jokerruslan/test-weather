﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Weather.Common
{
	public static class CustomMessagesHelper
	{
		public static readonly string ErrorCity = "City can not be null or empty";

		public static readonly string UnexpectedResponseFromServer = "Unexpected response from server";

		public static readonly string UnexpectedError = "UnexpectedError";

		public static readonly string ErrorCityNotFound = "City not found";

		public static readonly string ErrorServerUnavailable = "Server Unavailable";

		static readonly HttpStatusCode[] ServerError = new HttpStatusCode[] { HttpStatusCode.InternalServerError, HttpStatusCode.BadGateway, HttpStatusCode.ServiceUnavailable };


		public static bool IsServerError(HttpStatusCode statusCode)
		{
			return ServerError.Contains(statusCode);
		}
	}
}
