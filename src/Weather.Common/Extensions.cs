﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Weather.Common
{
	public static class Extensions
	{
		public static string GetDescription(this System.Enum value)
		{
			var enumMember = value.GetType().GetMember(value.ToString()).FirstOrDefault();

			var descriptionAttribute =
				enumMember == null
					? default(DescriptionAttribute)
					: enumMember.GetCustomAttribute(typeof(DescriptionAttribute)) as DescriptionAttribute;
			return
				descriptionAttribute == null
					? value.ToString()
					: descriptionAttribute.Description;
		}

		public static Func<T, object> OrderByColumn<T>(this IEnumerable<T> source, string column)
		{
			var sortingProperty = typeof(T).GetProperties().FirstOrDefault(x => string.Equals(x.Name, column, StringComparison.InvariantCultureIgnoreCase));
			
			return x => sortingProperty.GetValue(x);
		}
	}
}
