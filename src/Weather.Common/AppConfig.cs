﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Common
{
	public class AppConfig
	{
		public string AppId { get; set; }
		public string BaseApiUrl { get; set; }
	}
}
