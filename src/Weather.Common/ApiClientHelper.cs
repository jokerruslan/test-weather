﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Weather.Common
{
	public static class ApiClientHelper
	{
		public static readonly string WeatherClient = "WeatherClient";
		public static readonly string ForecastClient = "ForecastClient";
	}
}
